package TotalExtentReport;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ExtentReport {

	@Test
	public void loginTest()
	{
		System.out.println("Welcome to Amazon");

		ExtentHtmlReporter reporter = new ExtentHtmlReporter("./Extent_Report_Folder/learn.html");
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(reporter);
		
		ExtentTest logger = extent.createTest("login Test");
		logger.log(Status.INFO, "Login to Amazon");
		logger.log(Status.PASS, "Title verified");
		extent.flush();
		
		ExtentTest logger2 = extent.createTest("logoff Test");
		logger2.log(Status.FAIL, "Title verified");
		extent.flush();
	}
}
