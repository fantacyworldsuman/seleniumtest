package windowhandle;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class winhandling {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "E:\\workspace\\SeleniumTests\\lib\\ChromeDriver\\chromedriver.exe");
		WebDriver dr = new ChromeDriver();
		dr.manage().window().maximize();
		dr.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		dr.get("http://seleniumpractise.blogspot.com/");
		String parent = dr.getWindowHandle();
		System.out.println("The parent window id is : " + parent);
		
		dr.findElement(By.name("link1")).click();
		Set<String> allwindow = dr.getWindowHandles();
		System.out.println("The child window id is : " + allwindow);
		int count = allwindow.size();
		System.out.println("Total number of windows are : " + count);
		
		for(String child : allwindow)
		{
			if(!parent.equalsIgnoreCase(child))
			{
				dr.switchTo().window(child);
				dr.findElement(By.name("q")).sendKeys("Sachin Tendulkar");
			//	dr.findElement(By.name("btnK")).click(); 
				Thread.sleep(3000);
				dr.close();
			}
		}
		Thread.sleep(5000);
		dr.switchTo().window(parent);
		dr.findElement(By.xpath("//*[@id=\"post-body-6170641642826198246\"]/a[2]")).click();
		
	}

}
