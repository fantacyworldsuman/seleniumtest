package windowhandle;

import static org.testng.Assert.fail;

import java.util.ArrayList;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Browser_Handling {

	static WebDriver dr;
	
	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "E:\\workspace\\SeleniumTests\\lib\\ChromeDriver\\chromedriver.exe");
		dr = new ChromeDriver();
		dr.manage().window().maximize();
		dr.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		dr.get("http://toolsqa.com/automation-practice-switch-windows/");
		
		String parent = dr.getWindowHandle();
		System.out.println("The parent window ID is : " + parent);
		dr.findElement(By.xpath("//*[@id=\'button1\']")).click();
		
		Set<String> allWindow = dr.getWindowHandles();
		ArrayList<String> tabs = new ArrayList<>(allWindow);
		dr.switchTo().window(tabs.get(1));
		dr.manage().window().maximize();
		Thread.sleep(3000);
		dr.findElement(By.xpath("//*[@id=\"primary-menu\"]/li[5]/a/span[1]/span/span")).click();
		dr.close();
		dr.switchTo().window(tabs.get(0));
		dr.findElement(By.xpath("//*[@id=\"content\"]/p[3]/button")).click();
	}

}
