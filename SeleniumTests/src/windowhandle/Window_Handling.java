package windowhandle;

import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Window_Handling {

	static WebDriver dr;
	
	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "E:\\workspace\\SeleniumTests\\lib\\ChromeDriver\\chromedriver.exe");
		dr = new ChromeDriver();
		dr.manage().window().maximize();
		dr.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		dr.get("http://toolsqa.com/automation-practice-switch-windows/");
		
		String parent = dr.getWindowHandle();
		System.out.println("The parent window ID is : " + parent);
		
		dr.findElement(By.xpath("//*[@id=\"content\"]/p[4]/button")).click();
		
		Set<String> twoWin = dr.getWindowHandles();
		System.out.println("The available 2 window IDs are : " + twoWin);
		
		for(String child : twoWin)
		{
			if(!parent.equalsIgnoreCase(child))
			{
				dr.switchTo().window(child);
				dr.findElement(By.xpath("//*[@id=\"primary-menu\"]/li[5]/a/span[1]/span/span")).click();
				Thread.sleep(4000);
				//dr.close();
	
				Set<String> allWin = dr.getWindowHandles();
				System.out.println("The available three window IDs are : " + allWin);
				
				for(String child1 : allWin)
				{
					if((!parent.equalsIgnoreCase(child1))&&(!child.equalsIgnoreCase(child1)))
					{
						dr.switchTo().window(child1);Thread.sleep(4000);
						dr.findElement(By.xpath("//*[@id=\"header_search\"]/input")).sendKeys("KiddoLO");
						Thread.sleep(4000); 
						dr.switchTo().window(parent);	Thread.sleep(3000);
						dr.findElement(By.xpath("//*[@id=\"button1\"]")).click();
						
						Set<String> allWindows = dr.getWindowHandles();
						System.out.println("All the available Window IDs are : " + allWindows);
						
						for(String child2 : allWindows)
						{
							if((!parent.equalsIgnoreCase(child2))&&(!child.equalsIgnoreCase(child2))&&(!child1.equalsIgnoreCase(child2)))
							{
								dr.switchTo().window(child2);
								dr.manage().window().maximize();Thread.sleep(3000);
								String title = dr.getTitle();
								System.out.println("The title for another window is : " + title);
								dr.switchTo().window(child1);
								dr.findElement(By.xpath("//*[@id=\"header_search\"]/input")).sendKeys("VESKitty");			
							}
						}
					}					
				}
			}
		}
	}

	
}

