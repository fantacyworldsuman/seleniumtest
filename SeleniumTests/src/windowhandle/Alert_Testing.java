package windowhandle;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Alert_Testing {
	static WebDriver dr;

	public static void main(String[] args) {

			System.setProperty("webdriver.chrome.driver", "E:\\workspace\\SeleniumTests\\lib\\ChromeDriver\\chromedriver.exe");
			dr = new ChromeDriver();
			dr.manage().window().maximize();
			dr.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			dr.get("http://www.ksrtc.in/oprs-web/guest/home.do?h=1");
			dr.findElement(By.xpath("//*[@id=\"searchBtn\"]")).click();
			
			Alert alert = dr.switchTo().alert();
			String text = alert.getText();
			System.out.println("The appeared text is : " + text);
			alert.accept();
			dr.findElement(By.xpath("//*[@id=\"fromPlaceName\"]")).sendKeys("hw");
	}

}
