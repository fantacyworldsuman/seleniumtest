package Calendar;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class WebTable {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "E:\\workspace\\SeleniumTests\\lib\\ChromeDriver\\chromedriver.exe");
		WebDriver dr = new ChromeDriver();
		dr.manage().window().maximize();
		dr.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		dr.get("https://www.makemytrip.com/");
		
		dr.findElement(By.xpath("//*[@id=\"hp-widget__depart\"]")).click();
		
		List<WebElement> dates = dr.findElements(By.xpath("//div[@class='dateFilter hasDatepicker']/div/div[1]/table//td"));
		
		int count = dates.size();
		System.out.println("Number of appeared date fields are : " + count + "\n");
		
		for(WebElement ele : dates)
		{
			String date = ele.getText();
			System.out.println("The appeared date is : " + date);
			if(date.equals("25"))
			{
				ele.click();
				break;
			}
		}
	}

}
