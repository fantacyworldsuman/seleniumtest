package test;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class Rabio_Checkbox extends FirstSeleniumTest
{
	public void radio()
	{
		dr.get("http://seleniumpractise.blogspot.in/2016/08/how-to-automate-radio-button-in.html");
		List<WebElement> rad = dr.findElements(By.xpath("//input[@name='lang' and @type='radio']"));
		
		for(int i=0; i<rad.size(); i++)
		{
			WebElement ele = rad.get(i);
			String value = ele.getAttribute("value");
			System.out.println("The radio buttons are: " + value);
			
			if(value.equals("PYTHON"))
			{
				ele.click();
				System.out.println("\n" + "The selected option is: " + value + "\n");
			}
		}
		
	}
	
	public void checkbox()
	{
		List<WebElement> check = dr.findElements(By.xpath("//input[@name='lang' and @type='checkbox']"));
		for(int j=0; j<check.size();j++)
		{
			WebElement ele1 = check.get(j);
			String id = ele1.getAttribute("id");
			System.out.println("\n" + "The checkboxes are: " + id);
			
			if(id.equals("code"))
			{
				ele1.click();
				System.out.println("\n" + "The selected option is: " + id + "\n");
			}
		}
	}
}
