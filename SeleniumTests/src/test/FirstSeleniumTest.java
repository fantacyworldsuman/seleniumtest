package test;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class FirstSeleniumTest {

	static String browser;
	static WebDriver dr;
	
	public static void main(String[] args) throws InterruptedException {

		setBrowser();
		setBrowserConfig();
		
	/*	FirstSeleniumTest fd= new FirstSeleniumTest();
		fd.runTest();
		
	/*	SecondSeleniumTest.download();
		
		SecondSeleniumTest dc = new SecondSeleniumTest();
		dc.Documentation();*/
		
		Rabio_Checkbox r_b = new Rabio_Checkbox();
		r_b.radio();
		
		Rabio_Checkbox c_b = new Rabio_Checkbox();
		c_b.checkbox();
	}
	
	
	public static void setBrowser(){
		browser = "Chrome";
	} 
	
	public static void setBrowserConfig()
	{
		if(browser.contains("Firefox")){
			System.setProperty("webdriver.gecko.driver", "E:\\workspace\\SeleniumTests\\lib\\FirefoxDriver\\geckodriver.exe");
			dr = new FirefoxDriver();
		} 
	
		if(browser.contains("Chrome")){
			System.setProperty("webdriver.chrome.driver", "E:\\workspace\\SeleniumTests\\lib\\ChromeDriver\\chromedriver.exe");
			dr = new ChromeDriver();
		}
	} 
	
	public void runTest() throws InterruptedException
	{
		/*dr.get("http://seleniumhq.org/");
		String a = dr.findElement(By.xpath("//*[@id=\'menu_download\']/a")).getText();
		System.out.println(a);
		//dr.quit();*/
		
		dr.get("https://www.facebook.com/");
//		dr.manage().timeouts().implicitlyWait(4, TimeUnit.SECONDS);
		
		WebElement month_dd = dr.findElement(By.id("month"));
		Select month = new Select(month_dd);
		
		WebElement selected_value0 = month.getFirstSelectedOption();
		System.out.println("Before selection selected value is: " + selected_value0.getText());
		month.selectByIndex(4);				Thread.sleep(3000);
		month.selectByValue("10");			Thread.sleep(3000);
		month.selectByVisibleText("Aug");	
		
		WebElement selected_value = month.getFirstSelectedOption();
		System.out.println("After selection selected value is: " + selected_value.getText());
		
		List<WebElement> month_list = month.getOptions();
		int total_months = month_list.size();
	
		System.out.println("Total Number of month is: " + total_months);
		
		for(WebElement ele: month_list)
		{
			String month_name = ele.getText();
			System.out.println("The months name are: " + month_name);
		}
	} 
	
}









