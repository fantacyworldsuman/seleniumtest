package Element_Highlighting;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import Utility.Helper;

public class Ele_hl {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "E:\\workspace\\Second_Selenium_Practice\\Chrome_Driver\\chromedriver.exe");
		WebDriver dr = new ChromeDriver();
		dr.get("http://www.facebook.com");
		
		WebElement mailid = dr.findElement(By.id("email"));
		Helper.Highlight_Element(dr, mailid);
		mailid.sendKeys("gmail");
		WebElement psword = dr.findElement(By.id("pass"));
		Helper.Highlight_Element(dr, psword);
		psword.sendKeys("password");
	}

}
