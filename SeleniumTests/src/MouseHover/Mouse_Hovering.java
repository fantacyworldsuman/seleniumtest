package MouseHover;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Mouse_Hovering {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "E:\\workspace\\Second_Selenium_Practice\\Chrome_Driver\\chromedriver.exe");
		WebDriver dr = new ChromeDriver();
		dr.get("http://seleniumpractise.blogspot.com/2016/08/how-to-perform-mouse-hover-in-selenium.html");
		WebElement ele = dr.findElement(By.xpath("//*[@id=\"post-body-4229879368008023176\"]/div[1]/div[2]/button"));
		
		Actions ac = new Actions(dr);
		ac.moveToElement(ele).perform();
		
		List<WebElement>el = dr.findElements(By.xpath("//div[@class='dropdown-content']//a"));
		System.out.println("Total elements are: " + el.size());
		for(WebElement elem : el)
		{
			String sr = elem.getAttribute("innerHTML");
			if(sr.equalsIgnoreCase("Appium"))
			{
				elem.click();
			}
			else
			{
				System.out.println("Element name is: " + sr);
			}
		}		
	}
}
