package anotherListener;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Successful_Login {

	protected static WebDriver cp;
	
    @BeforeClass
    public static void Driver_Creation() {
    	System.setProperty("webdriver.chrome.driver", "E:\\workspace\\SeleniumTests\\lib\\ChromeDriver\\chromedriver.exe");
		cp = new ChromeDriver();
		cp.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
		cp.get("https://pi.cashpoint.com/");
		//cp.manage().window().maximize();
    }
    
	@Test(priority=0)
	public static void Calling()
	{
		Test_Output.Login();
		Test_Output.Logout();
	}
	@Test(priority=1)
	public static void man()
	{
		SecondOutput.LoginAgain();
	}


}
