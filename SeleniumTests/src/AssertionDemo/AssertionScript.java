package AssertionDemo;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AssertionScript {

	@Test
	public void Test1() throws InterruptedException
	{
		System.setProperty("webdriver.gecko.driver", "E:\\workspace\\SeleniumTests\\lib\\FirefoxDriver\\geckodriver.exe");
		WebDriver dr = new FirefoxDriver();
		dr.manage().window().maximize();
		dr.get("http://www.makemytrip.com");

		//Error Message Verification
		dr.findElement(By.xpath("//*[@id=\"searchBtn\"]")).click();	Thread.sleep(1000);
		String error_msg = dr.findElement(By.xpath("//*[@id=\'hp-widget__sTo_invalid_error\']/span[2]")).getText();
		System.out.println("The Alert Message is: " + error_msg);
		String expected_msg="Enter a valid 'Destination City' or select from the list.";
		Assert.assertEquals(error_msg, expected_msg);
		
		
		//Page Title Verification
		String site_title = dr.getTitle();
		System.out.println("Page Title is: " + site_title);	
		//String Expected_Title= "MakeMyTrip - #1 Travel Website 50% OFF on Hotels, Flights & Holiday";
		//Assert.assertEquals(site_title, Expected_Title);

		Assert.assertTrue(site_title.contains("MakeMyTrip"));
		//Assert.assertFalse(site_title.contains("sdfs"));
		
		System.out.println("Test Completed");
		
	}
}
