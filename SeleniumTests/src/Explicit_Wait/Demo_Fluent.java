package Explicit_Wait;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.google.common.base.Function;
import org.openqa.selenium.support.ui.FluentWait;

public class Demo_Fluent {

	static WebDriver dr;
	public static void main(String[] args) {

		System.setProperty("webdriver.gecko.driver", "E:\\workspace\\SeleniumTests\\lib\\FirefoxDriver\\geckodriver.exe");
		dr = new FirefoxDriver();
		dr.get("http://seleniumpractise.blogspot.com/2016/08/how-to-use-explicit-wait-in-selenium.html");
		dr.findElement(By.xpath("//button[@onclick='timedText()']")).click();
		
	//	Demo_Fluent.Explicit_Wait();
		Demo_Fluent.Fluent_Wait();
	}
	
	public static void Explicit_Wait(){
		
		WebDriverWait wait = new WebDriverWait(dr, 6);
		WebElement ele = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='WebDriver']")));
		
		boolean status = ele.isDisplayed();
		if(status)
		{
			System.out.println("The element is displayed.");
		}
		else
		{
			System.out.println("The element is not displayed");
		}
	}
	
	public static void Fluent_Wait(){
		
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(dr)
				.withTimeout(30, TimeUnit.SECONDS)
				.pollingEvery(2, TimeUnit.SECONDS)
				.ignoring(NoSuchElementException.class);
		
		WebElement el = wait.until(new Function <WebDriver, WebElement>()
		{
			public WebElement apply(WebDriver dr)
			{
				WebElement elem = dr.findElement(By.xpath("//*[@id=\"demo\"]"));
				String value = elem.getAttribute("innerHTML");
				if(value.equalsIgnoreCase("WebDriver"))
				{
					return elem;
				}
				else
				{
					System.out.println("Text which is coming on screen is: " + value);
					return null;
				}
			}
		} );
		System.out.println("Is element displayed? " + el.isDisplayed());
	}
}
