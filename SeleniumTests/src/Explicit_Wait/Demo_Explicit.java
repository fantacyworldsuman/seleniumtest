package Explicit_Wait;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Demo_Explicit {

	public static void main(String[] args) {

		System.setProperty("webdriver.gecko.driver", "E:\\workspace\\SeleniumTests\\lib\\FirefoxDriver\\geckodriver.exe");
		WebDriver dr = new FirefoxDriver();

		dr.get("http://seleniumpractise.blogspot.com/2016/08/how-to-use-explicit-wait-in-selenium.html");
	
		dr.findElement(By.xpath("//button[@onclick='timedText()']")).click();
		
		WebDriverWait wait = new WebDriverWait(dr, 6);
		WebElement ele = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='WebDriver']")));
		
		boolean status = ele.isDisplayed();
		if(status)
		{
			System.out.println("Element is displayed.");
		}
		else
		{
			System.out.println("Element is not displayed.");

		}
	}

}
