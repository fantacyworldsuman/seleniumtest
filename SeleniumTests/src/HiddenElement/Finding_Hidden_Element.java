package HiddenElement;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Finding_Hidden_Element {

	public static void main(String[] args) {

		System.setProperty("webdriver.chrome.driver", "E:\\workspace\\Second_Selenium_Practice\\Chrome_Driver\\chromedriver.exe");
		WebDriver dr = new ChromeDriver();
		dr.get("http://seleniumpractise.blogspot.com/2016/08/how-to-automate-radio-button-in.html");
		List<WebElement>ele = dr.findElements(By.id("male"));
		int size = ele.size();
		for(int i=0; i<size; i++)
		{
			WebElement elem = ele.get(i);
			int x_value = elem.getLocation().getX();
			if(x_value!=0)
			{
				elem.click();
				break;
			}
		}
		
	}

}
