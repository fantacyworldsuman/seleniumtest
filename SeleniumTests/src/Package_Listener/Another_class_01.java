package Package_Listener;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Another_class_01 {
	
 static WebDriver dr;
	
	@Test
	public static void googleTitle()
	{
		System.setProperty("webdriver.chrome.driver", "E:\\workspace\\SeleniumTests\\lib\\ChromeDriver\\chromedriver.exe");
		dr = new ChromeDriver();
		dr.manage().timeouts().implicitlyWait(6, TimeUnit.SECONDS);
		dr.get("https://www.google.com");
		//System.out.println("Appeared title is : " + dr.getTitle());
	}

	@Test
	public void verification()
	{
		System.out.println("Appeared title is : " + dr.getTitle());
		//Assert.assertTrue(true);
	}
}
