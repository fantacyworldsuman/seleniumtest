package Package_Listener;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestNG_Listener_01 implements ITestListener{

	@Override
	public void onTestStart(ITestResult result) {
		System.out.println("Test Case started and the details are : " + result.getName());

	}

	@Override
	public void onTestSuccess(ITestResult result) {
		System.out.println("Test Case succeeded and the details are : " + result.getName());

	}

	@Override
	public void onTestFailure(ITestResult result) {
		System.out.println("Test Case failed and the details are : " + result.getName());
	}
	
	

	@Override
	public void onTestSkipped(ITestResult result) {
		System.out.println("Test Case skipped and the details are : " + result.getName());

	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		
	}

	@Override
	public void onStart(ITestContext context) {
		
	}

	@Override
	public void onFinish(ITestContext context) {
		
	}

}
