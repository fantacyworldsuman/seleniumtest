package Headless_with_Log4j;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public class htmlunitdriver {

	public static void main(String[] args) {

		Logger logger = Logger.getLogger("htmlunitdriver");
		PropertyConfigurator.configure("Log4j.properties");
		
		WebDriver dr = new HtmlUnitDriver();
		
		dr.get("https://www.facebook.com");
		logger.info("App opening");
	    System.out.println("The title is : " + dr.getTitle());
	    logger.info("Title fetching");		
	}

}
