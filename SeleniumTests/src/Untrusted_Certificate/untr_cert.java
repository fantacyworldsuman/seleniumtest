package Untrusted_Certificate;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class untr_cert {

	public static void main(String[] args) {
		
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);

		//System.setProperty("webdriver.chrome.driver", "E:\\workspace\\SeleniumTests\\lib\\ChromeDriver\\chromedriver.exe");
		//WebDriver dr = new ChromeDriver();
		
		System.setProperty("webdriver.gecko.driver", "E:\\workspace\\SeleniumTests\\lib\\FirefoxDriver\\geckodriver.exe");
		WebDriver dr = new FirefoxDriver();
		dr.manage().window().maximize();
		//dr.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		dr.get("https://www.cacert.org");

		
	}

}
