package ForHer;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class BootstrapLogin {

	public static void main(String[] args) throws InterruptedException {

		System.setProperty("webdriver.chrome.driver", "E:\\workspace\\SeleniumTests\\lib\\ChromeDriver\\chromedriver.exe");
		WebDriver dr = new ChromeDriver();
		dr.manage().window().maximize();

		dr.get("https://www.goibibo.com");
		dr.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		dr.findElement(By.xpath("//a[@id='get_sign_up']")).click();
		
		dr.switchTo().frame("authiframe");
		dr.findElement(By.xpath("//input[@id='authMobile']")).sendKeys("8902445015");
		Thread.sleep(3000);

		dr.findElement(By.xpath("//div[@class='headerBox col-md-12 col-sm-12 col-xs-12']")).click();
		dr.switchTo().defaultContent();
	}
}