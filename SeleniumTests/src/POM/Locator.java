package POM;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

public class Locator {

	WebDriver dr;
	
	@FindBy(how = How.ID, using = "username")
	WebElement username;
	@FindBy(how = How.ID, using = "password")
	WebElement password;
	@FindBy(how = How.ID, using = "submit")
	WebElement login;
	
	public Locator(WebDriver driver)
	{
		this.dr = driver;
	}
	
	public void userLogin(String unm, String pwd)
	{
		username.sendKeys(unm);
		password.sendKeys(pwd);
		login.click();
	}
}
