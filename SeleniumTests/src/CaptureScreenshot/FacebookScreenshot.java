package CaptureScreenshot;


import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import Library.Utility;

public class FacebookScreenshot{
	
	@Test
	public void capscrshot() throws IOException
	{
		System.setProperty("webdriver.gecko.driver", "E:\\workspace\\SeleniumTests\\lib\\FirefoxDriver\\geckodriver.exe");
		WebDriver dr = new FirefoxDriver();
		dr.manage().window().maximize();

		dr.get("http://www.makemytrip.com");
		Utility.capturescreenshot(dr,"SiteOpen1");
	
		dr.findElement(By.xpath("//*[@id=\"js-switch__option\"]/div[2]/label")).click();
		Utility.capturescreenshot(dr,"RoundTrip1");
		
	}

}
