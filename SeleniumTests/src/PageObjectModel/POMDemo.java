package PageObjectModel;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

public class POMDemo {

	@Test
	public void initialization()
	{
		System.setProperty("webdriver.chrome.driver", "E:\\workspace\\SeleniumTests\\lib\\ChromeDriver\\chromedriver.exe");
		WebDriver dr = new ChromeDriver();
		dr.get("http://172.17.3.13/");
		
		Stored_Locators stlc = new Stored_Locators(dr);
		
		stlc.u_name();
		stlc.psward();
		stlc.Login();
	}

	
}
