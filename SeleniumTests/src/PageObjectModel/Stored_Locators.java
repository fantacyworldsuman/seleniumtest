package PageObjectModel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Stored_Locators {

	WebDriver dr;
	
	By username = By.xpath("//*[@id=\"input_0\"]");
	By password = By.xpath("//*[@id=\"input_1\"]");
	By login = By.xpath("//*[@id=\"login-form\"]/button");
	
	public Stored_Locators(WebDriver dr)
	{
		this.dr=dr;
	}
	
	public void u_name()
	{
		dr.findElement(username).sendKeys("_T_Admin");
	}
	
	public void psward()
	{
		dr.findElement(password).sendKeys("$123#LordTest");
	}
	
	public void Login()
	{
		dr.findElement(login).click();;
	}
}
