/**
 * 
 */
package Extent_Report;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.LogStatus;



public class Verify_Title {

	@Test
	public static void verifyTitle()
	{
		ExtentReports extent = ExtentReports.get(Verify_Title.class);
		extent.init("E:\\Suman\\Softwares\\Cell\\Extent_Report\\Report Path\\First_Report.html", true);
		extent.startTest("Verify the page title");
		
		System.setProperty("webdriver.chrome.driver", "E:\\workspace\\SeleniumTests\\lib\\ChromeDriver\\chromedriver.exe");
		WebDriver dr = new ChromeDriver();
		extent.log(LogStatus.INFO, "Chrome Driver initialized.");
		dr.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		dr.get("https://pi.cashpoint.com/");
		extent.log(LogStatus.INFO, "Application has opened.");
		String title = dr.getTitle();
		extent.log(LogStatus.INFO, "Fetching the Title.");
		System.out.println("Title is : " + title);
		extent.log(LogStatus.INFO, "Printing the Title.");
		Assert.assertTrue(title.contains("Partnerinfo"));
		extent.log(LogStatus.INFO, "Verification of the Title.");
		
		extent.attachScreenshot("E:\\Suman\\Softwares\\Cell\\Extent_Report\\Screenshot Path\\original.jpg");
		extent.endTest();
	}
}
